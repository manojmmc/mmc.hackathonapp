export class tableColumn {

  displayText: string;
  prop: string;
  isVisible?: boolean;
  constructor(displayText:string,prop:string,isVisible:boolean=true)
  {
   this.displayText=displayText;
   this.prop=prop;
   this.isVisible=isVisible;

  }
}
