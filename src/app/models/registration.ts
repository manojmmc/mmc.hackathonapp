export class UserRegistartion {
  "id" : number=0;
  "orgName": string="";
  "address": string="";
  "emailID": string="";
  "password": string="";
  "mobileNo": string="";
  "roleID": number=0;
  "status": boolean= true;
}

export class Employee {
  "id" : number=0;
  "firstName": string="";
  "middleName": string="";
  "surName": string="";
  "orgName": string="";
  "address": string="";
  "emailID": string="";
  "mobileNo": string="";
  "employerId":number= 0;
  "isPostedonFHIR":boolean= true;
  "add1": string="";
  "add2": string="";
  "add3": string="";
  "city": string="";
  "country": string="";
}


export class EmployeeClaim {
  "id" : number=0;
  "claimNo": string="";
  "claimDate": Date= new Date();
  "claimAmt": number=0;
  "employeeId": number=0;
  "status": string="Submited";
  "priority": string="Normal";
  "type": string="";
}
