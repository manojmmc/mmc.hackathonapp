import { Component, OnInit } from '@angular/core';
import { UserRegistartion } from '../models/registration';
import { tableColumn } from '../models/tablecolumn';
import { User } from '../models/user';
import { AuthenticationService } from '../services/authentication.service';
import { RegistrationApiService } from '../services/registration-api.service';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import {EmployeProfileComponent} from '../employe-profile/employe-profile.component';
import { UserProfileComponent } from '../user-profile/user-profile.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
headers :tableColumn[]=[];
rows:any[] = [];
  currentUser: UserRegistartion;
    users = [];

    constructor(
        private authenticationService: AuthenticationService,
        private api:RegistrationApiService,
        private matDialog: MatDialog,private router: Router

    ) {
        this.currentUser = this.authenticationService.currentUserValue;
    }

  ngOnInit(): void {
    this.headers=[];
    this.headers.push(new tableColumn("id","id",false));
    this.headers.push(new tableColumn("Org Name","orgName"));
    this.headers.push(new tableColumn("Address","address"));
    this.headers.push(new tableColumn("Email","emailID"));
    this.headers.push(new tableColumn("Mobile","mobileNo"));
    this.headers.push(new tableColumn("Role","roleName"));
    this.api.getAllAdmisByRoleId(this.currentUser.roleID, this.currentUser.id).subscribe((data)=>{

      if(Array.isArray( data))
      {
        data.forEach( (value)=> {
            let row:any={};
        this.headers.forEach(a=>{
          row[a.prop]=value[a.prop];
        });
        row['check']=false;
         this.rows.push(row);
      });

      }
  });
  }

  setAll(data:any)
  {
   console.log(data);
  }

  viewDetailCall(row:any)
  {
   console.log(row);
   const dialogRef = this.matDialog.open(UserProfileComponent, {
    "width": '800px',
    "backdropClass": 'custom-dialog-backdrop-class',
    "panelClass": 'custom-dialog-panel-class',
    "maxHeight": '600vh',
    "data": { pageValue: row },
    "autoFocus": false
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
    });


  }
  addUser()
  {
    this.router.navigate(['/register']);

  }

}
