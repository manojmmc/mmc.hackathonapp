import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { UserRegistartion } from '../models/registration';
import { tableColumn } from '../models/tablecolumn';
import { AuthenticationService } from '../services/authentication.service';
import { RegistrationApiService } from '../services/registration-api.service';

@Component({
  selector: 'app-claim-list',
  templateUrl: './claim-list.component.html',
  styleUrls: ['./claim-list.component.css']
})
export class ClaimListComponent implements OnInit {
  @Input() employeID:number=0;
  @Output("onUpdateClaimClick") onUpdateClaimClick: EventEmitter<any> = new EventEmitter();
  currentUser: UserRegistartion;
  headers :tableColumn[]=[];
  rows:any[] = [];
  constructor(
    private authenticationService: AuthenticationService,
    private api:RegistrationApiService,private router: Router
   ) {

      this.currentUser = this.authenticationService.currentUserValue;

     }

  ngOnInit(): void {
    this.loadClaimList();
  }


  loadClaimList()
   {
    this.headers=[];
    this.rows = [];
    this.headers.push(new tableColumn("claimId","claimId",false));
    this.headers.push(new tableColumn("Name","firstName"));
    this.headers.push(new tableColumn("Last Name","surName"));
    this.headers.push(new tableColumn("Claim No","claimNo"));
    this.headers.push(new tableColumn("Claim Amount","claimAmt"));
    this.headers.push(new tableColumn("Current Status","status"));
    this.headers.push(new tableColumn("Mob No","mobileNo"));
    this.headers.push(new tableColumn("Priority","priority"));
    this.headers.push(new tableColumn("Type","type"));
    this.headers.push(new tableColumn("Employer","orgName"));

    let empId=this.employeID;
    // if(this.employeID>0)
    // {
    //   empId=this.employeID;
    // }


    this.api.getCalimByEmpid(empId).subscribe((data)=>{

      if(Array.isArray( data))
      {
        data.forEach( (value)=> {
            let row:any={};
        this.headers.forEach(a=>{
          row[a.prop]=value[a.prop];
        });
        row['check']=false;
         this.rows.push(row);
      });

      }
  });

   }

   onUpdateClaim(row:any)
   {
    // console.log(row);
    // this.router.navigate(['/claimUpdate',row.id]);
    this.onUpdateClaimClick.emit(row);
   }

}
