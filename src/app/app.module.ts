import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { RegisterComponent } from './register/register.component';
import { DashboardComponent } from './dashboard/dashboard.component';
// import { MatTableModule } from '@angular/material/table';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Ng2TelInputModule } from 'ng2-tel-input';
import { UserListComponent } from './user-list/user-list.component';
import { GridTableComponent } from './grid-table/grid-table.component';
import { EmployeProfileComponent } from './employe-profile/employe-profile.component';
import { EmpListComponent } from './emp-list/emp-list.component';
import { EmpRegistrationComponent } from './emp-registration/emp-registration.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { JsonViewerComponent } from './json-viewer/json-viewer.component';
import { ClaimListComponent } from './claim-list/claim-list.component';
import { ClaimRegistrationComponent } from './claim-registration/claim-registration.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    DashboardComponent,
    UserListComponent,
    GridTableComponent,
    EmployeProfileComponent,
    EmpListComponent,
    EmpRegistrationComponent,
    UserProfileComponent,
    JsonViewerComponent,
    ClaimListComponent,
    ClaimRegistrationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatCheckboxModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    BrowserAnimationsModule,
    Ng2TelInputModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
