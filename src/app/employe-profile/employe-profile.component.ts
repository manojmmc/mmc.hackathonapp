import { Component, OnInit, Optional, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Employee } from '../models/registration';
import { RegistrationApiService } from '../services/registration-api.service';

@Component({
  selector: 'app-employe-profile',
  templateUrl: './employe-profile.component.html',
  styleUrls: ['./employe-profile.component.css']
})
export class EmployeProfileComponent implements OnInit {
  empData:Employee=new Employee();
  id:number=0;
  constructor(
    private api:RegistrationApiService,
    public dialogRef: MatDialogRef<EmployeProfileComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    private router: Router

  ) {

   this.id=data.pageValue.id;
   if(this.id>0)
   {
    this.loadEmpData(this.id);
   }

   }

  ngOnInit(): void {
  }

  loadEmpData(id:number)
  {

    this.api.getEmpById(id).subscribe((data:any)=>{
     if(data)
     {
      this.empData.id=data['id'];
      this.empData.firstName=data['firstName'];
      this.empData.middleName=data['middleName'];
      this.empData.surName=data['surName'];
      this. empData.orgName=data['orgName'];
      this.empData.address=data['add1'] + ' '+ data['add2'] + ' '+ data['add3'] +','+ data['city'];
      this.empData.emailID=data['emailID'];
      this.empData.mobileNo=data['mobileNo'];
     }

  });
  }

  closeDialog() {
    this.dialogRef.close({ event: 'close', data: "" });
  }

  addClaim()
  {
    this.closeDialog();
    this.router.navigate(['/claimRegistration',this.id]);
  }

  onUpdateClaim(row:any)
  {
    this.closeDialog();
    console.log(row);
    this.router.navigate(['/claimUpdate',row.claimId]);
  }

}
