import { HttpClient } from '@angular/common/http';
import { Employee, EmployeeClaim, UserRegistartion } from '../models/registration';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { BehaviorSubject, Observable, ReplaySubject } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({ providedIn: 'root' })

export class RegisterService {
    private currentUserSubject: BehaviorSubject<UserRegistartion>;
    public currentUser: Observable<UserRegistartion>;


constructor(private http: HttpClient) {
    let c=localStorage.getItem('currentUser');
    let test= c!=null ?JSON.parse (c):'';
    this.currentUserSubject = new BehaviorSubject<UserRegistartion>(test);
    this.currentUser = this.currentUserSubject.asObservable();
}
public get currentUserValue(): UserRegistartion {
    return this.currentUserSubject.value;
}

registration(data:UserRegistartion) {

    return this.http.post<any>(`${environment.apiURL}/Registration`,data)
        .pipe(map(userData => {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            let user=new UserRegistartion();
            user.id=userData.id;
            user.orgName=userData.orgName;
            user.address=userData.address;
            user.emailID=userData.emailID;
            user.password=userData.password;
            user.mobileNo=userData.mobileNo;
            user.roleID=userData.roleID;


           // Redirect to login pag
            return user;
        }));
}

empregistration(data: Employee){

    return this.http.post<any>(`${environment.apiURL}/Employee`,data)
        .pipe(map(userData => {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            let user=new Employee();
            user.id=userData.id;
            user.firstName=userData.firstName;
            user.middleName=userData.middileName;
            user.surName=userData.surName;
            user.orgName=userData.orgName;
            user.address=userData.address;
            user.emailID=userData.emailID;
            user.mobileNo=userData.mobileNo;
            user.add1=userData.add1;
            user.add2=userData.add2;
            user.add3=userData.add2;
            user.city=userData.city;
            user.country=userData.country;
            return user;
        }));
}


claimregistration(data: EmployeeClaim){

  return this.http.post<any>(`${environment.apiURL}/Claim`,data)
      .pipe(map(userData => {
        debugger;
         if(userData)
         {



         }
      }));
}


}
