import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, ReplaySubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../models/user';
import { environment } from '../../environments/environment';
import { UserRegistartion } from '../models/registration';


@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<UserRegistartion>;
    public currentUser: Observable<UserRegistartion>;

    constructor(private http: HttpClient) {
        let c=localStorage.getItem('currentUser');
        let test= c!=null ?JSON.parse (c):'';
        this.currentUserSubject = new BehaviorSubject<UserRegistartion>(test);
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): UserRegistartion {
        return this.currentUserSubject.value;
    }

    login(username:string, password:string) {

        return this.http.post<any>(`${environment.apiURL}/Login`, {
          "username": username,
          "password": password
        })
            .pipe(map(user => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                let us=new UserRegistartion();
                us.id=user.id;
                us.orgName=user.orgName;
                us.address=user.address;
                us.emailID=user.emailID;
                us.password=user.password;
                us.mobileNo=user.mobileNo;
                us.roleID=user.roleID;

                localStorage.setItem('currentUser', JSON.stringify(us));
                this.currentUserSubject.next(us);
                return us;
            }));
    }

    logout() {
        // remove user from local storage and set current user to null
        localStorage.removeItem('currentUser');

         this.currentUserSubject.next(new UserRegistartion());
    }

   

}
