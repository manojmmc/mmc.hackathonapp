import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class RegistrationApiService {

  constructor(private http:HttpClient) { }

  getData(){
    return this.http.get(`${environment.apiURL}/Login`);
  }
  getAllAdmis(){
    return this.http.get(`${environment.apiURL}/Registration/GetAllAdmins`);
  }

  getAllAdmisByRoleId(roleid:number, userid:number){
    return this.http.get(`${environment.apiURL}/Registration/GetAllAdminsByRoleId/`+roleid+'/'+ userid);
  }

  getAllEmp(){
    return this.http.get(`${environment.apiURL}/Registration/GetAllEmployess`);
  }
  getAllEmpByEmployerID(id:number){
    return this.http.get(`${environment.apiURL}/Employee/AllEmployeByEmployerId/`+id);
  }


  getClaimByClaimid(id:number){
    return this.http.get(`${environment.apiURL}/Employee/GetClaimDataByID/`+id);
  }


  getCalimByEmpid(id:number){
    if(id>0)
    return this.http.get(`${environment.apiURL}/Employee/ClaimByEmployeeId/`+id);
    else
    return this.http.get(`${environment.apiURL}/Employee/ClaimByEmployeerId/0`);
  }


  getEmpById(id:number){
    return this.http.get(`${environment.apiURL}/Employee/`+id);
  }

  getRegisterdUserById(id:number){
    return this.http.get(`${environment.apiURL}/Registration/`+id);
  }

  getEMPFHIRDataById(id:number){
    return this.http.get(`${environment.apiURL}/Person/GetPersonFHIR/`+id);
  }

  postEMPDataToToFHIRById(id:string){
    return this.http.get(`${environment.apiURL}/Person/PersonDataToFHIR/`+id);
  }


}
