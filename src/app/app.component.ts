import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserRegistartion } from './models/registration';
import { User } from './models/user';
import { AuthenticationService } from './services/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'MMC.HackathonApp';
  currentUser?: UserRegistartion;

  constructor(
      private router: Router,
      private authenticationService: AuthenticationService
  ) {
      this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  logout() {
      this.authenticationService.logout();
      this.router.navigate(['/login']);
  }

  getHello()
  {
    let msg="";
    if(this.currentUser?.roleID==1)
     {

      msg="Hello : "+this.currentUser.orgName + " You looged in as Role : Admin";
     }
     else
     if(this.currentUser?.roleID==2)
     {

      msg="Hello : "+this.currentUser.orgName + " You looged in as Role : Employer";
     }
     else
     if(this.currentUser?.roleID==3)
     {
      msg="Hello : "+this.currentUser.orgName + " You looged in as Role : Insurar";
     }
     else
     {
      msg="Hello : "+this.currentUser?.orgName + " You looged in as Role : Employee";
     }
     return msg;

  }

}
