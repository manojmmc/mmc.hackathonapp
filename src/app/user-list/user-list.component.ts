import { Component, OnInit } from '@angular/core';
import { RegistrationApiService } from '../services/registration-api.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  userData:any;
  constructor(private api:RegistrationApiService) { }

  ngOnInit(): void {
    this.api.getData().subscribe((data)=>{
      this.userData = data;
  });
}
}
