import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeProfileComponent } from '../employe-profile/employe-profile.component';
import { JsonViewerComponent } from '../json-viewer/json-viewer.component';
import { UserRegistartion } from '../models/registration';
import { tableColumn } from '../models/tablecolumn';
import { AuthenticationService } from '../services/authentication.service';
import { RegistrationApiService } from '../services/registration-api.service';

@Component({
  selector: 'app-emp-list',
  templateUrl: './emp-list.component.html',
  styleUrls: ['./emp-list.component.css']
})
export class EmpListComponent implements OnInit {
  headers :tableColumn[]=[];
  currentUser: UserRegistartion;
  rows:any[] = [];
  // dialogRef:any;
  constructor(
    private authenticationService: AuthenticationService,
    private api:RegistrationApiService,
    private matDialog: MatDialog,
    private router: Router) {

      this.currentUser = this.authenticationService.currentUserValue;

     }

   loadList()
   {

    this.headers=[];
    this.rows = [];
    this.headers.push(new tableColumn("id","id",false));
    this.headers.push(new tableColumn("First Name","firstName"));
    this.headers.push(new tableColumn("Last Name","surName"));
    this.headers.push(new tableColumn("Mobile No","mobileNo"));
    this.headers.push(new tableColumn("Email","emailID"));
    this.headers.push(new tableColumn("Address","city"));
    this.headers.push(new tableColumn("Posted","isPostedonFHIR"));
    this.headers.push(new tableColumn("Employer","orgName"));

   let emperId=0;
   if(this.currentUser.roleID!=1)
   {
      emperId=this.currentUser.id;
   }

    this.api.getAllEmpByEmployerID(emperId).subscribe((data)=>{

      if(Array.isArray( data))
      {
        data.forEach( (value)=> {
            let row:any={};
        this.headers.forEach(a=>{
          row[a.prop]=value[a.prop];
        });
        row['check']=false;
         this.rows.push(row);
      });

      }
  });
   }




    ngOnInit(): void {
      this.loadList();

    }

    viewDetailCall(row:any)
    {
     console.log(row);
     const dialogRef = this.matDialog.open(EmployeProfileComponent, {
      "width": '3000px',
      "backdropClass": 'custom-dialog-backdrop-class',
      "panelClass": 'custom-dialog-panel-class',
      "maxHeight": '150vh',
      "data": { pageValue: row },
      "autoFocus": false
      });
      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed', result);
        // this.dialogValue = result.data;
      });


    }

     postEmpToFHIR(row:any)
    {
      this.api.postEMPDataToToFHIRById(row.id).subscribe((data)=>{
      if(data)
      {
        this.loadList();

      }
      });

    }
    showFHIRData(row:any)
    {
     console.log(row);
     const dialogRef = this.matDialog.open(JsonViewerComponent, {
      "width": '4000px',
      "backdropClass": 'custom-dialog-backdrop-class',
      "panelClass": 'custom-dialog-panel-class',
      "maxHeight": '50vh',
      "data": { pageValue: row },
      "autoFocus": false
      });
      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed', result);
        // this.dialogValue = result.data;
      });


    }

    setAll(data:any)
    {
     console.log(data);
    }

    addEmployee()
    {
      this.matDialog.closeAll();
      this.router.navigate(['/empRegistration']);

    }
}
