import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClaimRegistrationComponent } from './claim-registration.component';

describe('ClaimRegistrationComponent', () => {
  let component: ClaimRegistrationComponent;
  let fixture: ComponentFixture<ClaimRegistrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClaimRegistrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClaimRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
