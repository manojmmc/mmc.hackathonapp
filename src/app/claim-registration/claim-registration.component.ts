import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Employee, EmployeeClaim, UserRegistartion } from '../models/registration';
import { AuthenticationService } from '../services/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';
import { RegistrationApiService } from '../services/registration-api.service';
import { formatDate } from '@angular/common';
import { RegisterService } from '../services/register.service';
import { first } from 'rxjs';

@Component({
  selector: 'app-claim-registration',
  templateUrl: './claim-registration.component.html',
  styleUrls: ['./claim-registration.component.css']
})
export class ClaimRegistrationComponent implements OnInit {
  claimForm: FormGroup;
  currentLoogedInUser: UserRegistartion;
  loading = false;
  submitted = false;
  empId:number=0;
  claimId:number=0;
  empData:Employee=new Employee();
  empClaim:EmployeeClaim=new EmployeeClaim();
  // formBuilder: any;

  claimStatus: { option: string, value: string }[] = [
    {
      'option': 'New',
      'value': 'New'
    },
    {
      'option': 'Inprogress',
      'value': 'Inprogress'
    },
    {
      'option': 'Approved',
      'value': 'Approved'
    },
    {
      'option': 'Rejected',
      'value': 'Rejected'
    }
  ];

  claimPriority: { option: string, value: string }[] = [
    {
      'option': 'Normal',
      'value': 'Normal'
    },
    {
      'option': 'High',
      'value': 'High'
    },
    {
      'option': 'Urgent',
      'value': 'Urgent'
    }
  ];

  claimType: { option: string, value: string }[] = [
    {
      'option': 'Dental',
      'value': 'Dental'
    },
    {
      'option': 'Vision',
      'value': 'Vision'
    },
    {
      'option': 'OPD',
      'value': 'OPD'
    }
  ];

  constructor( private authenticationService: AuthenticationService
    , private formBuilder: FormBuilder,private _Activatedroute:ActivatedRoute
    ,private api:RegistrationApiService,
    private registerService:  RegisterService, private router: Router) {
    this.currentLoogedInUser = this.authenticationService.currentUserValue;
    this._Activatedroute.paramMap.subscribe(params => {
      debugger;
    let parameterValue=params.get('id');

    if(this.router.url.indexOf('claimUpdate')>0)
    {
      this.claimId = parameterValue !=null && parameterValue!="" ? parseInt( parameterValue) : 0;
      if(this.claimId>0)
      {
       this.loadClaimData(this.claimId);
      }

    }
    else
    {
      this.empId = parameterValue !=null && parameterValue!="" ? parseInt( parameterValue) : 0;
     if(this.empId>0)
     {
      this.loadEmpData(this.empId);
     }
     }
    });


    this.claimForm = this.formBuilder.group({
      claimNo:['', [Validators.required,Validators.pattern("^((\\+91-?)|0)?[0-9]{1,10}$")]],
      // claimDate:['', Validators.required],
      claimDate: [formatDate(new Date(), 'yyyy-MM-dd', 'en'), [Validators.required]],
      claimAmt:['', Validators.required],
      status: ['', Validators.required],
      priority: ['', Validators.required],
      type:['',[Validators.required]]
   });

   this.claimForm.controls['status'].setValue("New");
   this.claimForm.controls['priority'].setValue("Normal");
   this.claimForm.controls['type'].setValue("Dental");


  }

  loadClaimData(id:number)
  {
    this.api.getClaimByClaimid(id).subscribe((data:any)=>{
      if(data)
      {
       this.empData.id=data['id'];
       this.empData.firstName=data['firstName'];
       this.empData.middleName=data['middleName'];
       this.empData.surName=data['surName'];
       this. empData.orgName=data['orgName'];
       this.empData.emailID=data['emailID'];
       this.empData.mobileNo=data['mobileNo'];

       this.empClaim.id=data['claimId'];
       this.claimId=this.empClaim.id;

       this.empClaim.claimAmt=data['claimAmt'];
       this.claimForm.controls['claimAmt'].setValue(this.empClaim.claimAmt);
      //  this.empClaim.claimDate=data[''];
       this.empClaim.claimNo=data['claimNo'];
       this.claimForm.controls['claimNo'].setValue(this.empClaim.claimNo);
       this.empClaim.employeeId=data['id'];
       this.empId=this.empClaim.employeeId;
       this.empClaim.priority=data['priority'];
       this.claimForm.controls['priority'].setValue(this.empClaim.priority);

       this.empClaim.status=data['status'];
       this.claimForm.controls['status'].setValue(this.empClaim.status);

       this.empClaim.type=data['type'];
       this.claimForm.controls['type'].setValue(this.empClaim.type);

      }

   });

  }
  loadEmpData(id:number)
  {

    this.api.getEmpById(id).subscribe((data:any)=>{
     if(data)
     {
      this.empData.id=data['id'];
      this.empData.firstName=data['firstName'];
      this.empData.middleName=data['middleName'];
      this.empData.surName=data['surName'];
      this. empData.orgName=data['orgName'];
      this.empData.address=data['address'];
      this.empData.emailID=data['emailID'];
      this.empData.mobileNo=data['mobileNo'];
     }

  });
  }

  // setFormGroup()
  // {
  //   this.claimForm = this.formBuilder.group({
  //     claimNo:['', [Validators.required,Validators.pattern("^((\\+91-?)|0)?[0-9]{1,10}$")]],
  //     claimDate:['', Validators.required],
  //     claimAmt:['', Validators.required],
  //     status: ['', Validators.required],
  //     priority: [''],
  //     type:['',[Validators.required]]
  //  });

  // }


  ngOnInit(): void {
  }


  onSubmit()
  {
    this.submitted = true;
    if (this.claimForm.invalid) {
      return;
    }

    this.loading = true;
    let user=new EmployeeClaim();
    user.id=this.claimId;
    user.claimNo=this.claimForm.value.claimNo;
    user.claimDate= new Date(this.claimForm.value.claimDate);
    user.claimAmt= parseFloat(this.claimForm.value.claimAmt);
    user.status=this.claimForm.value.status;
    user.type=this.claimForm.value.type;
    user.priority=this.claimForm.value.priority;
    user.employeeId=this.empId;

    this.registerService.claimregistration(user)
    .pipe(first())
    .subscribe(
      {
         next: (result: any) => {
        this.router.navigate(['/emplist']);
        },
        error:(err: any) => {
          // this.alertService.error(error);
          this.loading = false;
      }
});

  }

}
