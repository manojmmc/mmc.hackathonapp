import { ClaimRegistrationComponent } from './claim-registration/claim-registration.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from '../app/login/login.component';
import { RegisterComponent } from '../app/register/register.component';
import { DashboardComponent } from '../app/dashboard/dashboard.component';
import { AuthGuard } from './helper/auth.gaurd';
import { UserListComponent } from './user-list/user-list.component';
import { EmpListComponent } from './emp-list/emp-list.component';
import { EmpRegistrationComponent } from './emp-registration/emp-registration.component';
import { ClaimListComponent } from './claim-list/claim-list.component';

const routes: Routes = [
  { path: '', component: DashboardComponent,canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  {path:'userlist',component:UserListComponent},
  {path:'emplist',component:EmpListComponent},
  {path:'claimlist',component:ClaimListComponent},
  {path:'empRegistration',component:EmpRegistrationComponent},
  {path:'claimRegistration/:id',component:ClaimRegistrationComponent},
  {path:'claimUpdate/:id',component:ClaimRegistrationComponent},
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
