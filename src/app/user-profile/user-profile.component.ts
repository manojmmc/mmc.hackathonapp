import { Component, Inject, OnInit, Optional } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserRegistartion } from '../models/registration';
import { tableColumn } from '../models/tablecolumn';
import { RegistrationApiService } from '../services/registration-api.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  userData:UserRegistartion=new UserRegistartion();
  id:number=0;
  headers :tableColumn[]=[];
  rows:any[] = [];
  constructor(
    private api:RegistrationApiService,
    public dialogRef: MatDialogRef<UserProfileComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any

  ) {

   this.id=data.pageValue.id;
   if(this.id>0)
   {
    this.loadData(this.id);
    this.loadEmpList(this.id);
   }

   }

  ngOnInit(): void {
  }


loadEmpList(id:number)
{
  this.headers=[];
  this.headers.push(new tableColumn("id","id",false));
  this.headers.push(new tableColumn("First Name","firstName"));
  this.headers.push(new tableColumn("Last Name","surName"));
  this.headers.push(new tableColumn("Org Name","orgName"));
  this.headers.push(new tableColumn("Mobile No","mobileNo"));
  this.headers.push(new tableColumn("Email","emailID"));
  this.headers.push(new tableColumn("Address","address"));
  let emplerID=0;
  if(id>0)
  {
    emplerID=id;

  }
  this.api.getAllEmpByEmployerID(emplerID).subscribe((data)=>{

    if(Array.isArray( data))
    {
      data.forEach( (value)=> {
          let row:any={};
      this.headers.forEach(a=>{
        row[a.prop]=value[a.prop];
      });
      row['check']=false;
       this.rows.push(row);
    });

    }
});

}

  loadData(id:number)
  {

    this.api.getRegisterdUserById(id).subscribe((data:any)=>{
     if(data)
     {
      this.userData.id=data['id'];
      // this.userData.firstname=data['firstName'];
      // this.empData.middlename=data['middleName'];
      // this.empData.surname=data['surName'];
      this.userData.orgName=data['orgName'];
      this.userData.address=data['address'];
      this.userData.emailID=data['emailID'];
      this.userData.mobileNo=data['mobileNo'];
     }

  });
  }

  closeDialog() {
    this.dialogRef.close({ event: 'close', data: "" });
  }

}
