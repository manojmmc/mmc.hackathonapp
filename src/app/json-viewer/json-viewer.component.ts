import { Component, Inject, OnInit, Optional } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RegistrationApiService } from '../services/registration-api.service';

@Component({
  selector: 'app-json-viewer',
  templateUrl: './json-viewer.component.html',
  styleUrls: ['./json-viewer.component.css']
})
export class JsonViewerComponent implements OnInit {
  jsonData=' {  }';
  id:number=0;
  constructor(
    private api:RegistrationApiService,
    public dialogRef: MatDialogRef<JsonViewerComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any

  ) {

    this.id=data.pageValue.id;
    if(this.id>0)
    {
     this.loadEmpFHIRData(this.id);
    }


   }

   loadEmpFHIRData(id:number)
   {
    this.api.getEMPFHIRDataById(id).subscribe((data:any)=>{
      if(data)
      {
        this.jsonData=JSON.stringify(data);
      }

     });

   }

  ngOnInit(): void {
  }

}
