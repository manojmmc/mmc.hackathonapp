import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { tableColumn } from '../models/tablecolumn';

@Component({
  selector: 'app-grid-table',
  templateUrl: './grid-table.component.html',
  styleUrls: ['./grid-table.component.css']
})
export class GridTableComponent implements OnInit {
  @Input() headers:any[]=[];
  @Input() rows:any[]=[];
  @Input() showCheckBox:boolean=true;
  @Input() showViewDetail:boolean=false;
  @Input() showJsonViewer:boolean=false;
  @Input() showPostTOFHIRServer:boolean=false;
  @Input() showUpdateClaim:boolean=false;
  @Output("onCheckAll") onCheckAll: EventEmitter<any> = new EventEmitter();
  @Output("onCheckBoxCheck") onCheckBoxCheck: EventEmitter<any> = new EventEmitter();
  @Output("onViewDetailClick") onViewDetailClick: EventEmitter<any> = new EventEmitter();
  @Output("onJsonViewerClick") onJsonViewerClick: EventEmitter<any> = new EventEmitter();
  @Output("onPostTOFHIRServerClick") onPostTOFHIRServerClick: EventEmitter<any> = new EventEmitter();
  @Output("onUpdateClaimClick") onUpdateClaimClick: EventEmitter<any> = new EventEmitter();

  visibalHeader:any[]=[];

  constructor() {



  }

  ngOnInit(): void {
    // this.visibalHeader=
      this.headers.forEach( (value)=> {
          if(value['isVisible'])
          {
            this.visibalHeader.push(value);
          }
          });
  }


  checkAllOptions()
  {
    let selectedRecord :any[]=[];
    if (this.rows.every(val => val['check'] == true))
    {
      this.rows.forEach(val => { val['check'] = false });
    }
    else
    {
       this.rows.forEach(val => { val['check'] = true; selectedRecord.push(val['id']); });
    }
    this.onCheckAll.emit(selectedRecord);
  }

  onViewDetailButtonClick(row:any)
  {
    this.onViewDetailClick.emit(row);
  }

   onJsonViewerButtonClick(row:any)
  {
    this.onJsonViewerClick.emit(row);
  }

  onPostTOFHIRServerButtonClick(row:any)
  {
    this.onPostTOFHIRServerClick.emit(row);
  }

  onUpdateClaimButtonClick(row:any)
  {
    this.onUpdateClaimClick.emit(row);
  }

  checkOption()
  {

  }

  getDynamicID(row:any)
  {
     return "FHIR"+'_'+row['id'];

  }

}
