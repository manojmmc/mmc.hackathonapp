import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { RegisterService } from '../services/register.service';
import { UserRegistartion } from '../models/registration';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  loading = false;
  roleID: { option: string, value: string }[] = [
    {
      'option': 'Select from list',
      'value': ''
    },
    {
      'option': 'Employeer',
      'value': '2'
    },
    {
      'option': 'Insurance',
      'value': '3'
    }
  ];

  submitted = false;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private registerService: RegisterService
) {
  this.registerForm = this.formBuilder.group({
    orgName: ['', Validators.required],
    address: ['', Validators.required],
    mobileNo: ['', [Validators.required,Validators.pattern("^((\\+91-?)|0)?[0-9]{1,10}$")]],
    roleID: ['', Validators.required],
    emailID:['',[Validators.required,Validators.email]],
    password: ['', [Validators.required, Validators.minLength(6)]]
 });


}

get f() { return this.registerForm.controls; }
  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      orgName: ['', Validators.required],
      address: ['', Validators.required],
      mobileNo: ['', [Validators.required,Validators.pattern("^((\\+91-?)|0)?[0-9]{1,10}$")]],
      roleID: ['', Validators.required],
      emailID:['',[Validators.required,Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
   });

  }

  onSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }

     this.loading = true;
     let user=new UserRegistartion();
     user.orgName=this.registerForm.value.orgName;
     user.address=this.registerForm.value.address;
     user.mobileNo=this.registerForm.value.mobileNo;
     user.roleID=Number(this.registerForm.value.roleID);
     user.emailID=this.registerForm.value.emailID;
     user.password=this.registerForm.value.password;

     this.registerService.registration(user)
            .pipe(first())
            .subscribe(
              data => {
                    this.router.navigate(['/login']);
                },
               error=> {
                    this.loading = false;
                })
this.registerForm.reset();
  }

}
