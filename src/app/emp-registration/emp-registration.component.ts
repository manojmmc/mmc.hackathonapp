import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Employee, UserRegistartion } from '../models/registration';
import { first } from 'rxjs/operators';
import { RegisterService } from '../services/register.service';
import { AuthenticationService } from '../services/authentication.service';


@Component({
  selector: 'app-emp-registration',
  templateUrl: './emp-registration.component.html',
  styleUrls: ['./emp-registration.component.css']
})
export class EmpRegistrationComponent implements OnInit {

  empregisterForm: FormGroup;
  currentLoogedInUser: UserRegistartion;
  loading = false;
  submitted = false;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private registerService:  RegisterService,
    private authenticationService: AuthenticationService,
) {
  this.currentLoogedInUser = this.authenticationService.currentUserValue;
  this.empregisterForm = this.formBuilder.group({
    firstName:['', Validators.required],
    middileName:['', Validators.required],
    surName:['', Validators.required],
    orgName: ['', Validators.required],
    mobileNo: ['', [Validators.required,Validators.pattern("^((\\+91-?)|0)?[0-9]{1,10}$")]],
    emailID:['',[Validators.required,Validators.email]],
    add1:['', Validators.required],
    add2:['', Validators.required],
    add3:['', Validators.required],
    city:['', Validators.required],
    country:['', Validators.required]
 });
}
get f() { return this.empregisterForm.controls; }

  ngOnInit(): void {
    this.empregisterForm = this.formBuilder.group({
      firstName:['', Validators.required],
      middileName:['', Validators.required],
      surName:['', Validators.required],
      mobileNo: ['', [Validators.required,Validators.pattern("^((\\+91-?)|0)?[0-9]{1,10}$")]],
      emailID:['',[Validators.required,Validators.email]],
      add1:['', Validators.required],
      add2:['', Validators.required],
      add3:['', Validators.required],
      city:['', Validators.required],
      country:['', Validators.required]
   });
  }
  onSubmit() {
    this.submitted = true;
    if (this.empregisterForm.invalid) {
      return;
    }

     this.loading = true;
     let user=new Employee();
     user.firstName=this.empregisterForm.value.firstName;
     user.middleName=this.empregisterForm.value.middileName;
     user.surName=this.empregisterForm.value.surName;
     user.mobileNo=this.empregisterForm.value.mobileNo;
     user.emailID=this.empregisterForm.value.emailID;
     user.add1=this.empregisterForm.value.add1;
     user.add2=this.empregisterForm.value.add2;
     user.add3=this.empregisterForm.value.add3;
     user.city=this.empregisterForm.value.city;
     user.country=this.empregisterForm.value.country;
     user.employerId=this.currentLoogedInUser.id;

     this.registerService.empregistration(user)
            .pipe(first())
            .subscribe(
              {
                 next: (result: any) => {
                this.router.navigate(['/emplist']);
                },
                error:(err: any) => {
                  // this.alertService.error(error);
                  this.loading = false;
              }
  });
}


}
