export const environment = {
  production: true,
  title: 'Prod Environment Heading',
  apiURL: 'http://mmcfhirapi-dev.us-west-2.elasticbeanstalk.com/api'
};
